import { Component } from '@angular/core';

/**
 * Generated class for the CommunityComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'community',
  templateUrl: 'community.html'
})
export class CommunityComponent {

  text: string;

  constructor() {
    console.log('Hello CommunityComponent Component');
    this.text = 'Hello World';
  }

}
