import { NgModule } from '@angular/core';
import { CommunityComponent } from './community/community';
@NgModule({
	declarations: [CommunityComponent],
	imports: [],
	exports: [CommunityComponent]
})
export class ComponentsModule {}
