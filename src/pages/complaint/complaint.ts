import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, Slides} from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import * as firebase from "firebase";
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Geolocation } from '@ionic-native/geolocation';
import {AgmMap} from "@agm/core";

class ComplaintModel{
  imgs:Array<any> = [];
  description:string = null;
  alias:string = null;
  timestamp:any;
}

@IonicPage()
@Component({
  selector: 'page-complaint',
  templateUrl: 'complaint.html',
})
export class ComplaintPage {
  @ViewChild(AgmMap) agmMap: AgmMap;
  @ViewChild(Slides) slides: Slides;
  position = {
    lat:0,
    lng:0,
    latSelected:0,
    lngSelected:0
  };

  private complaintCollection: AngularFirestoreCollection<ComplaintModel>;
  complaint = new ComplaintModel();
  imgs:Array<any> = [];
  base64:string = 'data:image/jpeg;base64,';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private camera: Camera,
    private afs: AngularFirestore,
    private geolocation: Geolocation
  ) {
    this.complaintCollection = afs.collection<ComplaintModel>('complaints', ref => ref.orderBy('timestamp'));
  }

  ngAfterViewInit() {
    this.slides.freeMode = true;
    this.slides.lockSwipes(true)
  }



  ionViewDidLoad() {
    this.agmMap.mapClick.subscribe(
      result=>{
        console.log(result);
        this.position.latSelected = result.coords.lat;
        this.position.lngSelected = result.coords.lng;
      }
    );

    this.geolocation.getCurrentPosition()
      .then((resp) => {
        console.log(resp);
        this.position.lat = resp.coords.latitude;
        this.position.lng = resp.coords.longitude;
      })
      .catch((error) => {
        console.log('Error getting location', error);
      });
  }


  takePhoto(){
    const options: CameraOptions = {
      quality: 40,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };

    this.camera.getPicture(options)
      .then(
      (imageData) => {
          this.imgs.push(imageData)
        },
      (err) => {
          alert('Error Camera')
        }
    );
  }

  save(){
    new Promise(async (resolve, reject)=>{
      let imgs = [];
      for (let img of this.imgs){
        await this.upload(img)
          .then(url=>{
            imgs.push(url)
          })
          .catch(error=>{
            reject(error)
          })
      }
      resolve(imgs)
    })
      .then((urls:Array<any>)=>{
      /*
        this.complaint.imgs = urls;
        this.complaint.timestamp = firebase.firestore.FieldValue.serverTimestamp();
        this.complaintCollection.add({...this.complaint})
          .then(result=>{
            alert('Save success');
            this.complaint = new ComplaintModel()
            this.imgs = []
          })
          .catch(error=>{
            alert('Save error')
          })
     */
      })

  }

  upload(img:string) {
    let name = new Date().valueOf();
    let storageRef = firebase.storage().ref();
    let uploadTask:firebase.storage.UploadTask = storageRef.child(`uploads/${name}`)
      .putString(img,'base64', {contentType:'image/jpeg'});

    return new Promise((resolve, reject)=>{
      uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
        {
          next:(snapshot) => {console.log(snapshot)},
          error:(error) => {reject(error)},
          complete:()=>{resolve(uploadTask.snapshot.downloadURL)}
        }
      )
    })
  }


  isValid(){
    return (this.imgs.length && this.complaint.description && this.complaint.alias)
  }
}
