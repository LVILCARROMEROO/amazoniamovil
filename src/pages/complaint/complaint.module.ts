import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComplaintPage } from './complaint';
import {AgmCoreModule} from "@agm/core";

@NgModule({
  declarations: [
    ComplaintPage,
  ],
  imports: [
    IonicPageModule.forChild(ComplaintPage),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCcWKH3-AHtn9KYb6FJgf8yRhSujFP54aA'
    })
  ],
})
export class ComplaintPageModule {}
