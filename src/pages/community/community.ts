import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';

/**
 * Generated class for the CommunityPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-community',
  templateUrl: 'community.html',
})
export class CommunityPage {
  complaints;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private _db: AngularFirestore
  ) {
  }

  ionViewDidLoad() {

    this.complaints = this._db.collection('/complaints').snapshotChanges()
      .map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      });

    // this.complaints = this._db.collection('/complaints').valueChanges();
    // console.log('asd')
  }


  pad(s) { return (s < 10) ? '0' + s : s; }

  convertDate(item) {
    var d = item.timestamp;
    if (d) {
      item.timestamp_format = [
        this.pad((d.getMonth() + 1)),
        this.pad(d.getDate()),
        d.getFullYear()].join('/') + ' ' +
        [
          this.pad(d.getHours()),
          this.pad(d.getMinutes()),
          this.pad(d.getSeconds())
        ].join(':');


      return item.timestamp_format
    }
    return '-'
  }

  goToDetail(item) {
    this.navCtrl.push('CommunityDetailPage', item);
  }


}
