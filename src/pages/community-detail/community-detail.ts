import {Component, ElementRef, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, Platform, ViewController} from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { AngularFirestore } from 'angularfire2/firestore';
import * as firebase from 'firebase';

/**
 * Generated class for the CommunityDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare let google: any;
@IonicPage()
@Component({
  selector: 'page-community-detail',
  templateUrl: 'community-detail.html',
})
export class CommunityDetailPage {
  @ViewChild('group1') g1:ElementRef;
  @ViewChild('group2') g2:ElementRef;
  @ViewChild('group3') g3:ElementRef;
  g1icon = 'ios-arrow-down';
  g2icon = 'ios-arrow-down';
  g3icon = 'ios-arrow-down';
  parent;
  @ViewChild('map') mapRef:ElementRef;
  map;
  marker;
  position:{lat,lng};
  comments;
  mycomment = "";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private geolocation: Geolocation,
    private viewCtrl: ViewController,
    private platform: Platform,
    private _db: AngularFirestore
  ) {
    this.parent =  this.navParams.data;
  }

  
  ionViewDidLoad() {
    if(this.parent.geopoint)
      this.loadWebMap(this.parent.geopoint._lat,this.parent.geopoint._long);

      this.openGroup1();
      this.openGroup2();
      this.getComments();
  }



  loadWebMap(lat=-12,lng=-77){
    this.position = {lat,lng};
    const location = new google.maps.LatLng(lat,lng);
    const options = {
      center:location,
      zoom:15,
      streetViewControl: false,
      fullscreenControl: false,
      mapTypeId:'terrain',
      zoomControlOptions: {
        position: google.maps.ControlPosition.TOP_RIGHT
      },
    };
    this.map = new google.maps.Map(this.mapRef.nativeElement,options);
    this.marker = new google.maps.Marker({position:location,map:this.map});
    // let marker = this.marker;
    // this.map.addListener('click', function(result) {
    //   marker.setPosition(result.latLng);
    // });

  }

  
  openGroup1(){
    let condition = (this.g1.nativeElement.style.display=='none');
    this.g1.nativeElement.style.display = condition ? 'block':'none';
    this.g1icon = condition ? 'ios-arrow-up':'ios-arrow-down';

  }
  openGroup2(){
    let condition = (this.g2.nativeElement.style.display=='none');
    this.g2.nativeElement.style.display = condition ? 'block':'none';
    this.g2icon = condition ? 'ios-arrow-up':'ios-arrow-down';
  }
  openGroup3(){
    let condition = (this.g3.nativeElement.style.display=='none');
    this.g3.nativeElement.style.display = condition ? 'block':'none';
    this.g3icon = condition ? 'ios-arrow-up':'ios-arrow-down';
  }

  getComments(){
    this.comments = this._db.collection('/comments', ref => ref.where('complaint', '==', this.parent.id)).valueChanges();
  }

  pad(s) { return (s < 10) ? '0' + s : s; }
  convertDate(item) {
    var d = item.timestamp;
    if(d){
      
    item.timestamp_format = [
      this.pad((d.getMonth() + 1)),
      this.pad(d.getDate()),
      d.getFullYear()].join('/') + ' ' +
      [
        this.pad(d.getHours()),
        this.pad(d.getMinutes()),
        this.pad(d.getSeconds())
      ].join(':');

    return item.timestamp_format
  }
    return '-'
  }


  sendComment(e){
    let newcomment = {timestamp: firebase.firestore.FieldValue.serverTimestamp(), comment: "", complaint:""};
    newcomment.comment = this.mycomment;
    newcomment.complaint = this.parent.id;
    console.log(e, 'asdasdasd', this.mycomment)
    let id = this._db.createId()
    this._db.collection('/comments').doc(id).set(newcomment);
  }
}
