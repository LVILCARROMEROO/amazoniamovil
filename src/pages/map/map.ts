import {Component, ElementRef, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, Platform, ViewController} from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

declare let google: any;

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {
  @ViewChild('map') mapRef:ElementRef;
  map;
  marker;
  position:{lat,lng};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private geolocation: Geolocation,
    private viewCtrl: ViewController,
    private platform: Platform
  ) {
  }

  ionViewDidLoad() {
    this.geolocation.getCurrentPosition()
      .then((resp) => {
        let lat = resp.coords.latitude;
        let lng = resp.coords.longitude;
        this.loadWebMap(lat,lng);
      })

  }

  loadWebMap(lat,lng){
    this.position = {lat,lng};
    const location = new google.maps.LatLng(lat,lng);
    const options = {
      center:location,
      zoom:15,
      streetViewControl: false,
      fullscreenControl: false,
      mapTypeId:'terrain',
      zoomControlOptions: {
        position: google.maps.ControlPosition.TOP_RIGHT
      },
    };
    this.map = new google.maps.Map(this.mapRef.nativeElement,options);
    this.marker = new google.maps.Marker({position:location,map:this.map});
    let marker = this.marker;
    this.map.addListener('click', function(result) {
      marker.setPosition(result.latLng);
    });

  }

  goMyPosition(){
    this.geolocation.getCurrentPosition()
      .then((resp) => {
        let lat = resp.coords.latitude;
        let lng = resp.coords.longitude;
        const location = new google.maps.LatLng(lat,lng);
        this.marker.setPosition(location);
      })
  }

  ready(){
    this.viewCtrl.dismiss(this.position)
  }

  dismiss(){
    this.viewCtrl.dismiss();
  }

}
