import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Complaint2Page } from './complaint2';

@NgModule({
  declarations: [
    Complaint2Page,
  ],
  imports: [
    IonicPageModule.forChild(Complaint2Page),
  ],
})
export class Complaint2PageModule {}
