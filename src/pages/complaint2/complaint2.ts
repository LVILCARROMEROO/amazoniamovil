import {Component, ElementRef, ViewChild} from '@angular/core';
import {
  AlertController, IonicPage, Loading, LoadingController, ModalController, NavController,
  NavParams
} from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';
import { GalleryModal } from 'ionic-gallery-modal';
import firebase from "firebase"
import {environment} from "../../strings/constants";
import {Complaint} from "../../strings/models";
import {AngularFirestore, AngularFirestoreCollection} from "angularfire2/firestore";

const animal_confinement = ['Jaulas','Bolsas','Cajas','Encadenado'];
const animal_status = ['Vivo','Muerto','Lastimado'];
const place_types = ['Carretera','Mercado ilícito'];
const animal_ages = ['Cria', 'Joven', 'Adulto'];
const animal_species = [
  { text: 'Aves', types : ['Loros','Pihuicho','Tucan','Papagayos'] },
  { text: 'Mamíferos', types : ['Monos','Tigrillo','Armadillos']},
  { text: 'Reptiles', types : ['Cocodrilo','Lagartijas','Tortugas','Serpientes','Iguanas']},
  { text: 'Anfibios', types : ['Ranas','Sapos']},
  { text: 'Insectos', types : ['Mariposas','Escarabajos','Tarantulas']},
];


@IonicPage()
@Component({
  selector: 'page-complaint2',
  templateUrl: 'complaint2.html',
})
export class Complaint2Page {
  @ViewChild('group1') g1:ElementRef;
  @ViewChild('group2') g2:ElementRef;
  g1icon = 'ios-arrow-down';
  g2icon = 'ios-arrow-down';

  position: {lat,lng} = {lat:null, lng: null};
  photos:Array<{url:string}> = [];
  complaint: Complaint = new Complaint();
  consts = {
    place_types,
    animal_ages,
    animal_species,
    animal_specie_types:[],
    animal_status,
    animal_confinement,
  };
  complaintCollection: AngularFirestoreCollection<Complaint>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private geolocation: Geolocation,
    private camera: Camera,
    private imagePicker: ImagePicker,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private afs: AngularFirestore,
  ) {
    this.complaintCollection = this.afs.collection('complaints')
  }

  ionViewDidLoad() {
    firebase.initializeApp(environment.firebase);
    this.geolocation.getCurrentPosition()
      .then((resp) => {
        this.position.lat = resp.coords.latitude;
        this.position.lng = resp.coords.longitude;
      })
  }

  openMap(){
    const modal = this.modalCtrl.create('MapPage',{});
    modal.present()
  }

  openCamera(){
    const options: CameraOptions = {
      quality: 40,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };

    this.camera.getPicture(options).then(
      (imageData) => {
        let base64Image = `data:image/jpeg;base64,${imageData}`;
        this.photos.unshift({url:base64Image})
      },
      (err) => {console.log('Error Camera')}
    );

  }

  openGallery(){
    const options:ImagePickerOptions = {
      quality: 40,
      outputType: 1,
      maximumImagesCount: 1
    };
    this.imagePicker.getPictures(options).then(
      imgs => {
        for (let img of imgs) {
          let base64Image = `data:image/jpeg;base64,${img}`;
          this.photos.unshift({url:base64Image})
        }
      },
      err => alert(JSON.stringify(err))
    );
  }

  openModalGallery(index){
    let modal = this.modalCtrl.create(GalleryModal, {
      photos: this.photos,
      initialSlide: index
    });
    modal.present();
    modal.onDidDismiss((position)=>{
      if (position) this.position = position
    })
  }

  confirmDeleteImage(index){
    let alert = this.alertCtrl.create({
      title: '¿Desea eliminar esta imagen?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {}
        },
        {
          text: 'Si',
          handler: () => {
            this.deleteImage(index)
          }
        }
      ]
    });
    alert.present();
  }

  deleteImage(index){
    this.photos.splice(index,1)
  }


  save(){
    let loading = this.presentLoading()
    new Promise(async (resolve, reject)=>{
      let imgs = [];
      for (let img of this.photos){
        await this.upload(img.url)
          .then(url=>imgs.push(url))
          .catch(error=>reject(error))
      }
      resolve(imgs)
    }).then((urls:Array<any>)=>{
      console.log(this.position)
      let id = this.afs.createId();
      this.complaint.imgs = urls;
      this.complaint.timestamp = firebase.firestore.FieldValue.serverTimestamp();
      this.complaint.geopoint._lat = this.position.lat;
      this.complaint.geopoint._long = this.position.lng;
      this.complaintCollection.doc(id).set({...this.complaint})
        .then(()=>loading.dismiss())
        .catch(error=>{
          loading.dismiss()
          console.log(error)
          alert(JSON.stringify(error))
        })
    }).catch(error=>{
      loading.dismiss()
      console.log(error)
      alert(JSON.stringify(error))
    })
  }


  upload(img:string) {
    let name = new Date().valueOf();
    let storageRef = firebase.storage().ref();
    let uploadTask:firebase.storage.UploadTask;
    let path = `complaints/${name}`;
    uploadTask = storageRef.child(path).putString(img,'data_url');
    return new Promise((resolve, reject)=>{
      uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, {
          next:snapshot => {console.log(snapshot)},
          error:error => reject(error),
          complete:()=>{resolve(uploadTask.snapshot.downloadURL)}
        }
      )
    })
  }

  presentLoading():Loading {
    let loading = this.loadingCtrl.create({
      content: 'Guardando ...'
    });
    loading.present();
    return loading
  }


  changeAnimalSpecie(specie){
    this.complaint.d_animal_type = null;
    this.consts.animal_specie_types = specie.types
  }

  openGroup1(){
    let condition = (this.g1.nativeElement.style.display=='none');
    this.g1.nativeElement.style.display = condition ? 'block':'none';
    this.g1icon = condition ? 'ios-arrow-up':'ios-arrow-down';

  }
  openGroup2(){
    let condition = (this.g2.nativeElement.style.display=='none');
    this.g2.nativeElement.style.display = condition ? 'block':'none';
    this.g2icon = condition ? 'ios-arrow-up':'ios-arrow-down';

  }
}
