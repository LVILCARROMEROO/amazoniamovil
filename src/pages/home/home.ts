import { Component } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import * as firebase from "firebase";

export interface Item { name: string; timestamp:any}

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private itemsCollection: AngularFirestoreCollection<Item>;
  items: Observable<any[]>;
  name = '';
  constructor(private afs: AngularFirestore) {
    /*
    this.itemsCollection = afs.collection<Item>('items', ref => ref.orderBy('timestamp'));
    //this.items = this.itemsCollection.valueChanges();
    this.items = this.itemsCollection.snapshotChanges().map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      });
    });
    */
  }
  /*
  add(){
    let newItem = {name: this.name, timestamp:firebase.firestore.FieldValue.serverTimestamp()};
    this.itemsCollection.add(newItem)
  }

  update(item){
    this.itemsCollection.doc(`${item.id}`).update({name:this.name})
  }

  remove(item){
    this.itemsCollection.doc(`${item.id}`).delete()
  }

  upload(img:string) {
    let storageRef = firebase.storage().ref();
    let uploadTask:firebase.storage.UploadTask = storageRef.child(`uploads/nuevo`)
      .putString(img,'base64', {contentType:'image/jpeg'});
    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      {
        next:(snapshot) => {},
        error:(error) => {console.error(error)},
        complete:()=>{
          let url = uploadTask.snapshot.downloadURL;
        }
      }
    )
  }
  */
}
