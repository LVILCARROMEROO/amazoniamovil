import {Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AgmMap} from "@agm/core";

@IonicPage()
@Component({
  selector: 'page-maps',
  templateUrl: 'maps.html',
})
export class MapsPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
  ) {
  }

}
