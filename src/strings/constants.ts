export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyB7pKITcnfjthSfaw8IC68VpboaeEDORXY",
    authDomain: "amazonia-ab647.firebaseapp.com",
    databaseURL: "https://amazonia-ab647.firebaseio.com",
    projectId: "amazonia-ab647",
    storageBucket: "amazonia-ab647.appspot.com",
    messagingSenderId: "507630357909"
  }
};
