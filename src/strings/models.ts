export class Complaint {
  d_animal_color: string = null;
  d_animal_confinement: string = null;
  d_animal_name: string = null;
  d_animal_price: number = null;
  d_animal_quanty: number = null;
  d_animal_size: string = null;
  d_animal_status: string = null;
  d_animal_type: string = null;
  d_animal_age: string = null;
  d_animal_type_traffic: string = null;
  d_comment: string = null;
  d_trafficker_name: string = null;
  description: string = null;
  geopoint: {_lat, _long};
  imgs: Array<string[]> = [];
  timestamp: any;
  title: string = null;
  type: string = null;
  d_animal_specie: string = null;
  d_traffic_type: string = null;
  d_traffic_place_type: string = null;
  d_traffic_place_district: string = null;
  d_traffic_place_province: string = null;
  d_traffic_place_address: string = null;
}
