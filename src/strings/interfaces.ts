export interface Complaint {
  d_animal_color: string;
  d_animal_confinement: string;
  d_animal_name: string;
  d_animal_price: number;
  d_animal_quanty: number;
  d_animal_size: string;
  d_animal_status: string;
  d_animal_type: string;
  d_animal_type_traffic: string;
  d_comment: string;
  d_trafficker_name: string;
  description: string;
  geopoint: {_lat, _long};
  imgs: Array<string[]>;
  timestamp: any;
  title: string;
  type: string;
  d_animal_specie: string;
  d_animal_sub_specie: string;
  d_traffic_type: string;
  d_traffic_place_type: string;
  d_traffic_place_district: string;
  d_traffic_place_province: string;
  d_traffic_place_address: string;
}



